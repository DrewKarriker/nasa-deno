export { join } from "https://deno.land/std@0.73.0/path/mod.ts";
export { BufReader } from "https://deno.land/std@0.73.0/io/bufio.ts";
export { parse } from "https://deno.land/std@0.73.0/encoding/csv.ts";
export * as log from "https://deno.land/std/log/mod.ts";
export { Application, send, Router } from "https://deno.land/x/oak@v6.3.1/mod.ts";
export * as _ from "https://raw.githubusercontent.com/lodash/lodash/4.17.15-es/lodash.js";
export function habPlanets(planets) {
    return planets.filter((planet) => {
        const planetSize = Number(planet["koi_prad"]);
        const starSize = Number(planet["koi_smass"]);
        const starRadius = Number(planet["koi_srad"]);
        return planet["koi_disposition"] === "CONFIRMED" &&
            planetSize > 0.5 &&
            planetSize < 1.5 &&
            starSize > 0.78 &&
            starSize < 1.04 &&
            starRadius > 0.99 &&
            starRadius < 1.01;
    });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVwcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRlcHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxFQUFFLElBQUksRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUNyRSxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDckUsT0FBTyxLQUFLLEdBQUcsTUFBTSxrQ0FBa0MsQ0FBQztBQUd4RCxPQUFPLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNsRixPQUFPLEtBQUssQ0FBQyxNQUFNLHNFQUFzRSxDQUFDO0FBSTFGLE1BQU0sVUFBVSxVQUFVLENBQUUsT0FBc0I7SUFDaEQsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxFQUFFLEVBQUU7UUFDL0IsTUFBTSxVQUFVLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQzlDLE1BQU0sUUFBUSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQUM3QyxNQUFNLFVBQVUsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDOUMsT0FBTyxNQUFNLENBQUMsaUJBQWlCLENBQUMsS0FBSyxXQUFXO1lBQzlDLFVBQVUsR0FBRyxHQUFHO1lBQ2hCLFVBQVUsR0FBRyxHQUFHO1lBQ2hCLFFBQVEsR0FBRyxJQUFJO1lBQ2YsUUFBUSxHQUFHLElBQUk7WUFDZixVQUFVLEdBQUcsSUFBSTtZQUNqQixVQUFVLEdBQUcsSUFBSSxDQUFDO0lBQ3RCLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyJ9