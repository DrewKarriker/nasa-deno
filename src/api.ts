import { Router } from "./deps.ts";
import * as planets from "./planets.ts";
import * as launches from "./launches.ts";

const router = new Router();

router.get("/", (ctx) => {
  ctx.response.body = `
            i         __
            |         ()________________
        .-"""-.       ||::::::==========
       /  MMM  \\      ||::::::==========
      |  /~~~\\  |     ||::::::==========
      | ( ^.^ ) |     ||================
      \\__\\_U_/__/     ||================
       {_______}      ||================
     /' *       ''--._||
    /= . [NASA]     . { }:
   /  /|ooo     |''--'|| 
  (   )\\_______/      ||
   \\''\\/       \\      ||
    '-| ==    \\_|     ||
      /         |     ||
     |=   >\\  __/     ||
     \\     |- --|     ||
      \\ __| \\___/     ||
      _{__} _{__}     ||
     (    )(    )     ||
 ^^~  wwww  wwww  ~^^^~^^~~~^^^~^^^~^^^~^^~^
 Deno.js Mission Control API Started
 By @Drewlearns2
`;
});

//IMPORTS PLANETS PROJECT
router.get("/planets", (ctx) => {
  ctx.response.body = planets.getAllPlanets();
});

router.get("/launches", (ctx) => {
  ctx.response.body = launches.getAll();
});

router.get("/launches/:id", (ctx) => {
  if (ctx.params?.id) {
    const launchesList = launches.getOne(Number(ctx.params.id));
    if (launchesList) {
      ctx.response.body = launchesList;
    } else {
      ctx.throw(400, "No launch with that ID exists");
    }
  }
});

router.post("/launches", async (ctx) => {
  const body = await ctx.request.body();

  launches.addOne(await body.value); //IF YOU DON'T ADD "AWAIT" HERE IT WILL NOT RUN

  ctx.response.body = { success: true };
  ctx.response.status = 201;
});

router.delete("/launches/:id", (ctx) => {  //DONT FORGET THE "/" INFRONT OF "LAUNCHES/:ID"
  if (ctx.params?.id) {
    const result = launches.removeOne(Number(ctx.params.id));
    ctx.response.body = { success: result };
  }
});

export default router;
