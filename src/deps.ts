//STANDARD LIBRARY DEPENDENCIES:
export { join } from "https://deno.land/std/path/mod.ts";
export { BufReader } from "https://deno.land/std/io/bufio.ts";
export { parse } from "https://deno.land/std/encoding/csv.ts";
export * as log from "https://deno.land/std/log/mod.ts";

//3RD PARTY DEPENDENCIES:
export { Application, send, Router } from "https://deno.land/x/oak@v5.0.0/mod.ts";
export * as _ from "https://deno.land/x/lodash@4.17.15-es/lodash.js";

//Moduler filter
type Planet = Record<string, string>;
export function habPlanets (planets: Array<Planet>){
  return planets.filter((planet) => {
    const planetSize = Number(planet["koi_prad"]);
    const starSize = Number(planet["koi_smass"]);
    const starRadius = Number(planet["koi_srad"]);
    return planet["koi_disposition"] === "CONFIRMED" &&
      planetSize > 0.5 &&
      planetSize < 1.5 &&
      starSize > 0.78 &&
      starSize < 1.04 &&
      starRadius > 0.99 &&
      starRadius < 1.01;
  });
}