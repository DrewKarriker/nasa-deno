import { _, log } from "./deps.ts";
import { flatMap }  from "https://deno.land/x/lodash@4.17.15-es/lodash.js";

interface Launch {
  flightNumber: number;
  mission: string;
  rocket: string;
  customers: Array<string>;
  launchDate: number;
  upcoming: boolean;
  success?: boolean;
  target?: string;
}
const launches = new Map<number, Launch>();

async function downloadLaunchData() {
  await log.setup({
    handlers: {
      console: new log.handlers.ConsoleHandler("DEBUG"),
    },
    loggers: {
      default: {
        level: "DEBUG",
        handlers: ["console"],
      },
    },
  });
  log.info("Downloading launch Data");
  const response = await fetch(
    "https://api.spacexdata.com/v3/launches",
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
      },
      // body: JSON.stringify(
      // )
    },
  );
  if (!response.ok) {
    log.warning("Problem downloading launch data.");
    throw new Error("Launch data download failed.");
  } else {
    const launchData = await response.json();
    // log.info(launchData);
    for (const launch of launchData) {
      const payloads = launch["rocket"]["second_stage"]["payloads"];
      const customers = flatMap(
        payloads,
        (payload: any) => {
          return payload["customers"]
        }
      );
      const flightData = {
        flightNumber: launch["flight_number"],
        mission: launch["mission_name"],
        rocket: launch["rocket"]["rocket_name"],
        launchDate: launch["launch_date_unix"], //definitely don't write "launchData" here or you will pull your hairs out
        upcoming: launch["upcoming"],
        success: launch["launch_success"],
        customers: customers,
      };

      launches.set(flightData.flightNumber, flightData);
      // log.info( JSON.stringify(flightData));
    };
  };
}
downloadLaunchData();
log.info(`Downloaded data for ${launches.size} SpaceX launches.`)

export function getAll(){
  return Array.from(launches.values());
}

export function getOne(id : number){
  if (launches.has(id)){
    return launches.get(id);
  }
}

export function addOne(data : Launch){
  launches.set(
    data.flightNumber, 
    Object.assign(data, 
      {
      upcoming: true,
      customers: ["Drew", "NASA"],
    }
    )
  )
}

export function removeOne(id: number){
  const aborted = launches.get(id)
  if (aborted){
    aborted.upcoming = false;
    aborted.success = false;
  }
  return aborted;
}
// deno run --allow-net=reqres.in mod.ts
// deno run --allow-net=api.spacexdata.com mod.ts
