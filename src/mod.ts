import { Application, log, send} from "./deps.ts";
import api from "./api.ts";

const app = new Application();
const PORT = 8000;

await log.setup({
  handlers: {
    console: new log.handlers.ConsoleHandler("INFO"),
  },
  loggers: {
    default: {
      level: "INFO",
      handlers: ["console"],
    },
  },
});

app.addEventListener('error', (event)=> {
  log.error(event.error);
})

app.use(async(ctx, next)=>{
  try {
    await next();
  }catch(err){
    ctx.response.body = "Internal Server Error";
    throw err;
  }
})

app.use(async (ctx, next) => {
  await next();
  const time = ctx.response.headers.get(`X-Response-Time`);
  log.info(`${ctx.request.method} ${ctx.request.url} ${time}`);
});

//RESPONSE INFORMATION
app.use(async (ctx, next) => {
  const start = Date.now(); // send the anscii art
  await next(); //awaits sending it to the requester
  const delta = Date.now() - start; // calculates the difference between response end time and start time
  ctx.response.headers.set(`X-Response-Time`, `${delta}ms`);
});

//ROUTER API.TS
app.use(api.routes());
app.use(api.allowedMethods());

//STATIC FILES
app.use(async (ctx) => {
  const filePath = ctx.request.url.pathname;
  const fileAllowList = [
    "/index.html",
    "/images/favicon.png",
    "/javascripts/script.js",
    "/stylesheets/style.css",
    "/videos/space.mp4"
  ];
  if (fileAllowList.includes(filePath)) {
    await send(ctx, filePath, {
      root: `${Deno.cwd()}/public`,
    });
  }
});

if (import.meta.main) {
  log.info(`Starting server on port ${PORT}`);
  app.listen({
    port: PORT,
  });
}
