import { _, BufReader, join, parse, habPlanets, log } from "./deps.ts";
import { pick }  from "https://deno.land/x/lodash@4.17.15-es/lodash.js";
//DEFINE TYPESCRIPT INTERFACE - ENSURES WE ARE COMPARING NUMBERS AND NOT STRINGS

// interface Planet {
//   [key: string]: string;
// } //REMOVED IN FAVOR OF "TYPE" BELOW

type Planet = Record<string, string>;

let planets : Array<Planet>;

async function getPlanets() {
  const path = join("data", "kepler_exoplanets_nasa.csv");
  const file = await Deno.open(path); //ALWAYS CLOSE AFTER OPENING
  const bufReader = new BufReader(file);
  const result = await parse(bufReader, { skipFirstRow: true, comment: "#" });
  Deno.close(file.rid); //ALWAYS CLOSE AFTER OPENING

  // log.info(planets);


  //FILTER PLANETS THAT ARE HABITABLE - ADD NUMBER() CONSTRUCTOR TO ALL THE PROPERTIES
  const habitablePlanets = habPlanets(result as Array<Planet>);

  return habitablePlanets.map((planet) => {
    return pick(
      planet,
      [
        "koi_prad", // planet radius
        "koi_smass", // mass of star
        "koi_srad", // radius of star
        "kepler_name", // name of planet
        "koi_count", // number of planets in solar system
        "koi_steff",
        "koi_period",
      ],
    );
  } // temp of star
  );
} // getPlanets();

planets = await getPlanets();

log.info(`${planets.length} planets found that may support life!`);

export function getAllPlanets() {
  return planets;
}